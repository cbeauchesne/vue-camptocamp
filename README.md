# [camptocamp.org](https://www.camptocamp.org)

[![Build](https://gitlab.com/cbeauchesne/vue-camptocamp/badges/master/build.svg)](https://gitlab.com/cbeauchesne/vue-camptocamp/pipelines)

On any OS, install [git](https://git-scm.com/) and [node.js](https://nodejs.org/en/). Then :

```bash
# Download camptocamp.org source code :
git clone https://github.com/c2corg/c2c_ui
cd vue-camptocamp

# Install dependencies. It's quite long, but you have to do it once.
npm install
```

And now, let's launch
```
npm run serve
```

:heart: http://localhost:8080 :heart:

Want to easily access to every dev tools on a fancy interface? Have a look on [the doc](https://github.com/c2corg/c2c_ui/blob/master/docs/development-environment.md)! :sunglasses:
