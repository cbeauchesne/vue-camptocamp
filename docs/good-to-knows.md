## Good to know

Here is a list of questions without answers, and hacks used to make things works.

Please add any point or hack when intention is not purely clear for a newbie that read your code.

* `v-tooltip` directive does not work on fort-awesome SVG icons (but why??)
  * so, most of this icons are wrapped in a span
* [CSS tootlip does not works when element parent got a `overflow:hidden`](https://github.com/chinchang/hint.css/issues/152#issuecomment-248810536)
  * do not use tooltip in this case :'(
