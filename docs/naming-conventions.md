### File and folder names

* folder's names are kebab-case
* Vue components are PascalCase
* Javascript
  * If the file describes a single object, it must be in PascalCase
  * otherwise, it must be in kebab-case

### Vue components name

Stick to VueJs conventions !

* Todo : vue doc link   

### Javascript names

Stick to JS convention

* Todo : JS doc link  
