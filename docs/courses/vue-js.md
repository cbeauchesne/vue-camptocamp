

## Prerequisites

* Installed [node](https://nodejs.org/en/)
* Basic knowledge of HTML/Javascript
* (Very) basic knowledge of bash/cmd command lines

## Install

### Vue CLI

We use the [Vue CLI](https://cli.vuejs.org/guide/installation.html) solution :

```bash
npm install -g @vue/cli
```

:info: `-g` stands for global, meaning that you will have to do it only once on your computer.

### First project

```bash
vue create
npm run serve
```
:heart: : http://localhost:8080

## Concepts

### Components

* [Mono file components](https://vuejs.org/v2/guide/single-file-components.html)
* `<template>` part
* `<script>` part
* `<style>` part

### State & Reactivity

* What is a state ?
* How reactivity works

### Interpolations ...

* https://vuejs.org/v2/guide/syntax.html#Interpolations

### ... and binding

* https://vuejs.org/v2/guide/syntax.html#Attributes

### Style

* Scoped, not scoped ?

## Must knows

### `v-if`/`v-show`

How to conditionally show/render a component ?

### `v-for`

Render a list of component

### `v-bind`

* bind values to attributes
* Shortand : `:`

### `v-on`

* Connect events
* Shortand : `@`

### `v-model`

## Vue CLI

* forget all command lines, except one : `vue ui`
* got to http://localhost:8000
* Serve
* install dependancies

## Links

* [Documentation](https://vuejs.org/v2/guide/index.html)
* [Component lifecycle diagram](https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram)
* [CLI documentation](https://cli.vuejs.org/guide/)
